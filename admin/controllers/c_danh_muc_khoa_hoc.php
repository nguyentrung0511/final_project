<?php
include ("models/m_danh_muc_khoa_hoc.php");
class c_danh_muc_khoa_hoc
{
    public function index()
    {
        $m_danh_muc = new m_danh_muc_khoa_hoc();
        $dmkh = $m_danh_muc->read_danh_muc();
        $dm_kh = 'views/danh_muc_khoa_hoc/v_danh_muc_khoa_hoc.php';
        include("templates/danh_muc_khoa_hoc/layout.php");
    }

    public function add_dm()
    {
        $m_danh_muc = new m_danh_muc_khoa_hoc();
        $add_dmkh = $m_danh_muc->read_danh_muc();
        if (isset($_POST["btnSave"])) {
            $id_dm = null;
            $ten_dm = $_POST["ten_danh_muc"];
            $trang_thai = $_POST["trang_thai"];


            foreach ($add_dmkh as $dm) {

                if ($ten_dm == $dm->ten_danh_muc_kh) {
                    echo "<script>alert('Tên danh mục bị trùng thêm không thành công');window.location='add_danh_muc_khoa_hoc.php'</script>";
                    return;
                }
            }
            $kq = $m_danh_muc->add_danh_muc($id_dm, $ten_dm, $trang_thai);
            if ($kq) {

                echo "<script>alert('Thêm không thành công');window.location='danh_muc_khoa_hoc.php'</script>";

            }

        }
        $add = 'views/danh_muc_khoa_hoc/add_dmkh.php';
        include("templates/danh_muc_khoa_hoc/layout_add.php");
    }

    public function edit_dm()
    {

        if (isset($_GET["id"])) {

            $id_dm=$_GET["id"];
            $m_danh_muc = new m_danh_muc_khoa_hoc();
            $read_id = $m_danh_muc->read_id_danh_muc($id_dm);
//            print_r($read_id) ;
//            echo $read_id[1];
//            die();
            $edit_dmkh = $m_danh_muc->read_danh_muc();
            if (isset($_POST["btnSave"])) {

                $ten_dm = $_POST["ten_danh_muc"];
                $trang_thai = $_POST["trang_thai"];
                foreach ($edit_dmkh  as $dm) {

                    if ($ten_dm == $dm->ten_danh_muc_kh) {
                        echo "<script>alert('Tên danh mục bị trùng thêm không thành công');window.location='edit_danh_muc_khoa_hoc.php'</script>";
                        return;
                    }
                }
                $kq = $m_danh_muc->edit_danh_muc( $ten_dm,$trang_thai,$id_dm);
                if ($kq) {

                    echo "<script>alert('Thêm thành công');window.location='danh_muc_khoa_hoc.php'</script>";

                }
            }
            $dm_kh = 'views/danh_muc_khoa_hoc/v_edit_danh_muc.php';
            include("templates/danh_muc_khoa_hoc/layout.php");
        }
    }
}
?>