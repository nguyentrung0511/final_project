<?php
include ("models/m_khoa_hoc.php");
include ("models/m_read_all.php");
class c_khoa_hoc{
    public function index()
    {
        $m_khoa_hoc= new m_khoa_hoc();
        $kh = $m_khoa_hoc->read_khoa_hoc();
        $khoa_hoc='views/khoa_hoc/v_khoa_hoc.php';
        include ("templates/khoa_hoc/layout.php");
    }
    public function add_khoa_hoc(){
           $m_all_danh_muc= new m_read_all();
           $all_dm=$m_all_danh_muc->read_all_danh_muc_khoa_hoc();
           $all_kh= $m_all_danh_muc->read_all_khoa_hoc();
           if(isset($_POST["btnSave"])) {
               $id_kh= null;
               $ten_kh=$_POST["ten_khoa_hoc"];
               $thoi_gian=$_POST["thoi_gian"];
               $id_danh_muc=$_POST["danh_muc"];
               $trang_thai=$_POST["trang_thai"];
               $hoc_phi=$_POST["hoc_phi"];


            foreach ($all_kh as $kh) {

                if ($ten_kh == $kh->ten_khoa_hoc) {
                    echo "<script>alert('Tên khóa học bị trùng thêm không thành công');window.location='add_khoa_hoc.php'</script>";
                    return;
                }
            }
            $m_khoa_hoc= new m_khoa_hoc();
            $kq = $m_khoa_hoc->add_khoa_hoc($id_kh,$ten_kh,$hoc_phi,$thoi_gian,$id_danh_muc,$trang_thai);
            if ($kq) {

                echo "<script>alert('Thêm không thành công');window.location='khoa_hoc.php'</script>";

            }

        }
        $khoa_hoc='views/khoa_hoc/v_add_khoa_hoc.php';
        include ("templates/khoa_hoc/layout.php");
    }
    public function edit_kh()
    {
        if (isset($_GET["id"])) {

            $id_kh = $_GET["id"];
            $m_khoa_hoc = new m_khoa_hoc();
            $read_id = $m_khoa_hoc->read_id_khoa_hoc($id_kh);
            $m_all_danh_muc= new m_read_all();
            $all_dm=$m_all_danh_muc->read_all_danh_muc_khoa_hoc();
//            $edit_kh = $m_khoa_hoc->read_khoa_hoc();
            if (isset($_POST["btnSave"])) {
                $ten_kh=$_POST["ten_khoa_hoc"];
                $thoi_gian=$_POST["thoi_gian"];
                $id_danh_muc=$_POST["danh_muc"];
                $trang_thai=$_POST["trang_thai"];
                $hoc_phi=$_POST["hoc_phi"];
//                foreach ($edit_kh as $khoa_hoc) {
//
//                    if ($ten_kh  == $khoa_hoc->ten_khoa_hoc) {
//                        echo "<script>alert('Tên danh mục bị trùng thêm không thành công');window.location='edit_khoa_hoc.php?id=".$id_kh."'</script>";
//                        return;
//                    }
//                }
                $kq = $m_khoa_hoc->edit_khoa_hoc($ten_kh,$hoc_phi,$thoi_gian,$id_danh_muc,$trang_thai,$id_kh);
                if ($kq) {

                    echo "<script>alert('Thêm thành công');window.location='khoa_hoc.php'</script>";

                }
            }
            $khoa_hoc = 'views/khoa_hoc/v_edit_khoa_hoc.php';
            include("templates/khoa_hoc/layout.php");
        }
    }

}
?>