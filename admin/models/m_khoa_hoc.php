<?php
require_once("database.php");
class m_khoa_hoc extends database
{
    public function read_khoa_hoc()
    {
        $sql = "SELECT kh.id, kh.ten_khoa_hoc,kh.hoc_phi,kh.thoi_gian,dm.ten_danh_muc_kh,kh.trang_thai 
                FROM khoa_hoc as kh , danh_muc_khoa_hoc as dm WHERE kh.id_danh_muc = dm.id 
                ORDER BY dm.ten_danh_muc_kh";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function add_khoa_hoc($id_kh,$ten_kh,$hoc_phi,$thoi_gian,$id_danh_muc,$trang_thai){
        $sql ="insert into khoa_hoc values(?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id_kh,$ten_kh,$hoc_phi,$thoi_gian,$id_danh_muc,$trang_thai));
    }
    public function read_id_khoa_hoc($id_kh){
        $sql = "select * from khoa_hoc where id=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id_kh));
    }
    public function edit_khoa_hoc($ten_kh,$hoc_phi,$thoi_gian,$id_danh_muc,$trang_thai,$id_kh)
    {
        $sql="update khoa_hoc set ten_khoa_hoc=?,hoc_phi=?,thoi_gian=?,id_danh_muc=?,trang_thai=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten_kh,$hoc_phi,$thoi_gian,$id_danh_muc,$trang_thai,$id_kh));
    }
}
