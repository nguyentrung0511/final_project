<?php
require_once("database.php");
class m_danh_muc_khoa_hoc extends database
{
    public function read_danh_muc()
    {
        $sql = "select * from danh_muc_khoa_hoc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_id_danh_muc($id_dm){
        $sql = "select * from danh_muc_khoa_hoc where id=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id_dm));
    }
    public function add_danh_muc($id_dm,$ten_dm,$trang_thai){
        $sql ="insert into danh_muc_khoa_hoc values(?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id_dm,$ten_dm,$trang_thai));
    }
    public function edit_danh_muc($ten_dm,$trang_thai,$id_dm)
    {
        $sql="update danh_muc_khoa_hoc set ten_danh_muc_kh=?,trang_thai=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten_dm,$trang_thai,$id_dm));
    }
}

?>