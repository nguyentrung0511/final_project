<?php
require_once("database.php");
class m_read_all extends database{
    public function read_all_danh_muc_khoa_hoc(){
        $sql = "select * from danh_muc_khoa_hoc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_khoa_hoc(){
        $sql = "select * from khoa_hoc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
}