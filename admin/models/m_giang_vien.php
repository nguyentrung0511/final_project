<?php
require_once("database.php");
class m_giang_vien extends database
{
    public function read_giang_vien()
    {
        $sql = "select * from giang_vien";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function add_giang_vien($id_gv,$ten_gv,$ngay_sinh,$gioi_tinh,$dia_chi,$so_dien_thoai,$email,$pass,$hinh_anh,$trang_thai){
        $sql ="insert into giang_vien values(?,?,?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id_gv,$ten_gv,$ngay_sinh,$gioi_tinh,$dia_chi,$so_dien_thoai,$email,$pass,$hinh_anh,$trang_thai));
    }
}

?>