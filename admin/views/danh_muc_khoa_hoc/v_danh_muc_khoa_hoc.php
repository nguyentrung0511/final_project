<?php
include("aside_dmkh.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Danh mục khóa học</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Danh mục khóa học</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="row">
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title m-b-0"><a href="add_danh_muc_khoa_hoc.php "><button type="button" class="btn btn-success btn-sm">Add New</button></a></h5>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Tên Danh Mục</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $dmkh as $dmkh){

                            ?>
                            <tr>
                                <th><?php echo $i++ ;?></th>
                                <td><?php echo $dmkh->ten_danh_muc_kh ;?></td>
                                <td><?php if($dmkh->trang_thai ==1){?>
                                        <span class="badge badge-pill badge-info float">Hoạt Động</span>
                                    <?php } if($dmkh->trang_thai ==0){?>
                                    <span class="badge badge-pill badge-warning float">Không Hoạt Động</span>
                                    <?php } ?>
                                </td>
                                <td><button type="button" class="btn btn-info btn-lg" onclick="window.location.href='edit_danh_muc_khoa_hoc.php?id=<?php echo $dmkh->id; ?>'"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger btn-lg"><i class="nav-icon fa fa-trash-alt"></i></button>
                                </td>
                            </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Main Footer -->