<?php
include("aside_giang_vien.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard v3</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">Tables</h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title m-b-0"><a href=" add_giang_vien.php"><button type="button" class="btn btn-success btn-sm">Add New</button></a></h5>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Tên Giảng Viên</th>
                                <th scope="col">Hình Ảnh</th>
                                <th scope="col">Ngày Sinh</th>
                                <th scope="col">Giới Tính</th>
                                <th scope="col">Địa chỉ</th>
                                <th scope="col">Số Điện Thoại</th>
                                <th scope="col">Email</th>
                                <th scope="col">Password</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=1;
                                foreach ($gv as $value)
                                {
                                    ?>
                            <tr>
                                <td><?php echo $i++;?></td>
                                <td><?php echo $value->ten_giang_vien;?></td>
                                <td><img src="../public/layout/imagesbanner/<?php echo $value->hinh_anh?>" style="width: 80px;"></td>
                                <td><?php echo $value->ngay_sinh;?></td>
                                <td><?php echo $value->gioi_tinh;?></td>
                                <td><?php echo $value->dia_chi;?></td>
                                <td><?php echo $value->so_dien_thoai;?></td>
                                <td><?php echo $value->email;?></td>
                                <td><?php echo $value->password;?></td>
                                <td><?php if($value->trang_thai ==1){?>
                                        <span class="badge badge-pill badge-info float">Đang Giảng Dạy</span>
                                    <?php } if($value->trang_thai ==0){?>
                                        <span class="badge badge-pill badge-warning float">Không Còn Giảng Dạy</span>
                                    <?php } ?>
                                </td>
                                <td><button type="button" class="btn btn-info btn-lg" onclick="window.location.href='edit_khoa_hoc.php?id=<?php echo $value->id; ?>'"> <i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger btn-lg"><i class="nav-icon fa fa-trash-alt"></i></button>
                                </td>
                            </tr>
                            <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Main Footer -->