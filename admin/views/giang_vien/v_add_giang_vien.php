<?php
include("aside_giang_vien.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Giảng Viên</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="giang_vien.php">Giảng Viên</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Thêm Giảng Viên</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form enctype="multipart/form-data" method="post">
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputProvince">Tên Giảng Viên</label>
                    <input type="text" class="form-control" name="ten_giang_vien" autofocus="true" autocomplete="on" required>
                </div>

                    <label for="exampleInputProvince">Ngày Sinh</label>
                    <div class="input-group">
                        <input type="date" class="form-control mydatepicker" name="ngay_sinh" placeholder="mm/dd/yyyy">
                    </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Giới Tính</label>
                    <select class="form-control" name="gioi_tinh">
                        <option value="Nam">Nam</option>
                        <option value="Nữ">Nữ</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Địa Chỉ</label>
                    <input type="text" class="form-control" name="dia_chi" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Số Điện Thoại</label>
                    <input type="text" class="form-control" name="so_dien_thoai" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Email</label>
                    <input type="text" class="form-control" name="email" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Password</label>
                    <input type="text" class="form-control" name="password" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group row">
                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Hình</label>
                    <div class="col-sm-9">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="hinh_anh" required>
                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                        <option value="1">Đang Giảng Dạy</option>
                        <option value="0">Không Còn Giảng Dạy</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>


