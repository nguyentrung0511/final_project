<aside class="main-sidebar elevation-4 sidebar-light-primary">
    <!-- Brand Logo -->
    <a href="public/layout/index3.html" class="brand-link">
        <img src="public/layout/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="public/layout/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>



        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="danh_muc_tin_tuc.php" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Danh mục tin tức
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="tin_tuc.php" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Tin tức
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="khuyen_mai.php" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Khuyến mại
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="danh_muc_khoa_hoc.php" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Danh mục khóa học
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="khoa_hoc.php" class="nav-link">
                        <i class="nav-icon fas fa-tree"></i>
                        <p>
                            Khóa học
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="giang_vien.php" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            Giảng viên
                        </p>
                    </a>

                </li>
                <li class="nav-item">
                    <a href="lop_hoc.php" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Lớp học
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="hoc_vien.php" class="nav-link">
                        <i class="nav-icon fas fa-calendar-alt"></i>
                        <p>
                            Học viên
                        </p>
                    </a>
                </li>
                <li class="nav-item menu-open">
                    <a href="diem_thi.php" class="nav-link active">
                        <i class="nav-icon far fa-image"></i>
                        <p>
                            Điểm thi
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="dang_ky.php" class="nav-link">
                        <i class="nav-icon fas fa-columns"></i>
                        <p>
                            Đăng ký
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="hinh_thuc_thanh_toan.php" class="nav-link">
                        <i class="nav-icon far fa-envelope"></i>
                        <p>
                            Hình thức thanh toán
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="phan_quyen.php" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Phân quyền
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="lien_he.php" class="nav-link">
                        <i class="nav-icon far fa-plus-square"></i>
                        <p>
                            Liên hệ
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>