<?php
include("aside_khoa_hoc.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard v3</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">Tables</h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title m-b-0"><a href="add_khoa_hoc.php "><button type="button" class="btn btn-success btn-sm">Add New</button></a></h5>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Tên Khóa Học</th>
                                <th scope="col">Học Phí</th>
                                <th scope="col">Thời Gian</th>
                                <th scope="col">Danh mục</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $kh as $kh){

                                ?>
                                <tr>
                                    <th><?php echo $i++ ;?></th>
                                    <td><?php echo $kh->ten_khoa_hoc ;?></td>
                                    <td><?php echo $kh->hoc_phi ;?> $</td>
                                    <td><?php echo $kh->thoi_gian ;?> tháng</td>
                                    <td><?php echo $kh->ten_danh_muc_kh ;?></td>
                                    <td><?php if($kh->trang_thai ==1){?>
                                            <span class="badge badge-pill badge-info float">Active</span>
                                        <?php } if($kh->trang_thai ==0){?>
                                            <span class="badge badge-pill badge-warning float">Inactive</span>
                                        <?php } ?>
                                    </td>
                                    <td><button type="button" class="btn btn-info btn-lg" onclick="window.location.href='edit_khoa_hoc.php?id=<?php echo $kh->id; ?>'"> <i class="fa fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger btn-lg"><i class="nav-icon fa fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Main Footer -->