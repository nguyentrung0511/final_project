<?php
include("aside_khoa_hoc.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Danh mục khóa học</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="khoa_hoc.php">Danh mục khóa học</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Thêm Danh Mục Khóa Học</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="add_khoa_hoc.php">
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputProvince">Tên Khóa Học</label>
                    <input type="text" class="form-control" name="ten_khoa_hoc" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Học Phí</label>
                    <input type="text" class="form-control" name="hoc_phi" autofocus="true" autocomplete="on" placeholder="1000$" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Thời Gian</label>
                    <input type="text" class="form-control" name="thoi_gian" autofocus="true" autocomplete="on" placeholder="6 tháng" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Danh Mục Khóa Học</label>
                    <select class="form-control" name="danh_muc">
                        <?php
                        foreach ($all_dm as $value) {
                        ?>
                        <option value="<?php echo  $value->id;?>"><?php echo $value->ten_danh_muc_kh ;?></option>
                        <?php
                            }
                            ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                        <option value="1">Hoạt Đông</option>
                        <option value="0">Không Hoạt Động</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

