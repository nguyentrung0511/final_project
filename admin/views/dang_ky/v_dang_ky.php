<?php
include("aside_dang_ky.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Danh sách đăng ký</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Danh sách đăng ký3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">Danh sách đăng ký</h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Tên Người Đăng Ký</th>
                                <th scope="col">Số Điện Thoại</th>
                                <th scope="col">Email</th>
                                <th scope="col">Ngày Đăng Ký</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $dk as $dky){

                            ?>
                            <tr>
                                <td><?php echo $i++ ;?></td>
                                <td><?php echo $dky->name ;?></td>
                                <td><?php echo $dky->so_dien_thoai ;?></td>
                                <td><?php echo $dky->email ;?></td>
                                <td><?php echo $dky->ngay_dang_ky ;?></td>
                                <td><?php if($dky->trang_thai ==1){?>
                                        <span class="badge badge-pill badge-info float">Đã Xử lý</span>
                                    <?php } if($dky->trang_thai ==0){?>
                                        <span class="badge badge-pill badge-warning float">Chưa Xử lý </span>
                                    <?php } ?>
                                </td>
                                <td><button type="button" class="btn btn-info btn-lg" ><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger btn-lg"><i class="nav-icon fa fa-trash-alt"></i></button>
                                </td>
                            </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Main Footer -->
