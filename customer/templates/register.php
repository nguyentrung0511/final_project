<!doctype html>
<html class="no-js" lang="">


<!-- Mirrored from affixtheme.com/html/xmee/demo/register-5.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 29 Jun 2021 05:58:28 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Xmee | Login and Register Form Html Templates</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="public/register/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="public/register/css/bootstrap.min.css">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="public/register/css/fontawesome-all.min.css">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="public/register/font/flaticon.css">
    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="public/register/style.css">
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div id="wrapper" class="wrapper">
    <div class="fxt-template-animation fxt-template-layout5">
        <div class="fxt-bg-img fxt-none-767" data-bg-image="public/register/img/figure/bg5-l.jpg">
            <div class="fxt-intro">
                <div class="sub-title">Welcome To</div>
                <h1>Our xmee</h1>
                <p>Grursus mal suada faci lisis Lorem ipsum dolarorit ametion consectetur elit. Vesti ulum nec the dumm.</p>
            </div>
        </div>
        <div class="fxt-bg-color">
            <div class="fxt-header">
                <a href="login-5.html" class="fxt-logo"><img src="public/register/img/logo-5.png" alt="Logo"></a>
            </div>
            <div class="fxt-form">
                <form method="POST">
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-1">
                        <input type="text" class="form-control" name="name" placeholder="Full Name" required="required">
                        <i class="flaticon-user"></i>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-2">
                        <input type="email" class="form-control" name="email" placeholder="Your Email" required="required">
                        <i class="flaticon-envelope"></i>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-3">
                        <input type="text" class="form-control" name="so_dien_thoai" placeholder="Phone" required="required">
                        <i class="flaticon-padlock"></i>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-4">
                        <div class="fxt-content-between">
                            <button type="submit" name="btnSave" class="fxt-btn-fill">Register</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="fxt-footer">
                <ul class="fxt-socials">
                    <li class="fxt-facebook fxt-transformY-50 fxt-transition-delay-6"><a href="#" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="fxt-twitter fxt-transformY-50 fxt-transition-delay-7"><a href="#" title="twitter"><i class="fab fa-twitter"></i></a></li>
                    <li class="fxt-google fxt-transformY-50 fxt-transition-delay-8"><a href="#" title="google"><i class="fab fa-google-plus-g"></i></a></li>
                    <li class="fxt-linkedin fxt-transformY-50 fxt-transition-delay-9"><a href="#" title="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                    <li class="fxt-pinterest fxt-transformY-50 fxt-transition-delay-9"><a href="#" title="pinterest"><i class="fab fa-pinterest-p"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- jquery-->
<script src="public/register/js/jquery-3.5.0.min.js"></script>
<!-- Popper js -->
<script src="public/register/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="public/register/js/bootstrap.min.js"></script>
<!-- Imagesloaded js -->
<script src="public/register/js/imagesloaded.pkgd.min.js"></script>
<!-- Validator js -->
<script src="public/register/js/validator.min.js"></script>
<!-- Custom Js -->
<script src="public/register/js/main.js"></script>

</body>


<!-- Mirrored from affixtheme.com/html/xmee/demo/register-5.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 29 Jun 2021 05:58:39 GMT -->
</html>