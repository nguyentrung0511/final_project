<footer class="user_footer_wrap">
    <div class="sc_section margin_bottom_1_imp sc_footer_custom_bg1">
        <div class="sc_section_overlay">
            <div class="sc_section_content">
                <div class="sc_content content_wrap">
                    <div class="sc_section aligncenter width_70per">
                        <h2 class="sc_title sc_title_regular margin_top_05em">Schools &amp; Partners</h2>
                        We believe in offering the highest quality courses, created by schools and partners who share our commitment to excellence in teaching and learning, both online and in the classroom.
                    </div>
                    <div id="sc_section_2" class="sc_section margin_top_1_5em_imp margin_bottom_075em_imp height_75">
                        <div id="sc_section_2_scroll" class="sc_scroll sc_scroll_horizontal swiper-slider-container scroll-container height_75">
                            <div class="sc_scroll_wrapper swiper-wrapper">
                                <div class="sc_scroll_slide swiper-slide">
                                    <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp">
                                        <img src="public/layout/images/partners_01.jpg" alt="" />
                                    </figure>
                                    <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
                                        <img src="public/layout/images/partners_02.jpg" alt="" />
                                    </figure>
                                    <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
                                        <img src="public/layout/images/partners_03.jpg" alt="" />
                                    </figure>
                                    <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
                                        <img src="public/layout/images/partners_04.jpg" alt="" />
                                    </figure>
                                    <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
                                        <img src="public/layout/images/partners_05.jpg" alt="" />
                                    </figure>
                                    <figure class="sc_image alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
                                        <img src="public/layout/images/partners_06.jpg" alt="" />
                                    </figure>
                                </div>
                            </div>
                            <div id="sc_section_2_scroll_bar" class="sc_scroll_bar sc_scroll_bar_horizontal sc_section_2_scroll_bar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /Partners footer -->
<!-- Widgets Footer -->
<footer class="footer_wrap bg_tint_light footer_style_light widget_area">
    <div class="content_wrap">
        <div class="columns_wrap">
            <!-- Calendar widget -->
            <aside class="column-1_3 widget widget_calendar">
                <h5 class="widget_title">Calendar</h5>
                <table>
                    <thead>
                    <tr>
                        <th class="month_prev">
                            <a href="#" data-type="post,courses,tribe_events" data-year="2015" data-month="01" title="View posts for January 2015"></a>
                        </th>
                        <th class="month_cur" colspan="5">September <span>2015</span></th>
                        <th class="month_next">
                            <a href="#" data-month="10" data-year="2015" data-type="post,courses,tribe_events" title="View posts for October 2015"></a>
                        </th>
                    </tr>
                    <tr>
                        <th class="weekday" scope="col" title="Monday">Mon</th>
                        <th class="weekday" scope="col" title="Tuesday">Tue</th>
                        <th class="weekday" scope="col" title="Wednesday">Wed</th>
                        <th class="weekday" scope="col" title="Thursday">Thu</th>
                        <th class="weekday" scope="col" title="Friday">Fri</th>
                        <th class="weekday" scope="col" title="Saturday">Sat</th>
                        <th class="weekday" scope="col" title="Sunday">Sun</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="1" class="pad"><span class="day_wrap">&nbsp;</span></td>
                        <td class="day"><span class="day_wrap">1</span></td>
                        <td class="day"><span class="day_wrap">2</span></td>
                        <td class="day"><span class="day_wrap">3</span></td>
                        <td class="day"><span class="day_wrap">4</span></td>
                        <td class="day"><span class="day_wrap">5</span></td>
                        <td class="day"><span class="day_wrap">6</span></td>
                    </tr>
                    <tr>
                        <td class="day"><span class="day_wrap">7</span></td>
                        <td class="day"><span class="day_wrap">8</span></td>
                        <td class="day"><span class="day_wrap">9</span></td>
                        <td class="day"><a class="day_wrap" title="Post" href="#">10</a></td>
                        <td class="day"><span class="day_wrap">11</span></td>
                        <td class="day"><span class="day_wrap">12</span></td>
                        <td class="day"><span class="day_wrap">13</span></td>
                    </tr>
                    <tr>
                        <td class="day"><span class="day_wrap">14</span></td>
                        <td class="day"><span class="day_wrap">15</span></td>
                        <td class="day"><span class="day_wrap">16</span></td>
                        <td class="day"><span class="day_wrap">17</span></td>
                        <td class="day"><a class="day_wrap" title="Post" href="#">18</a></td>
                        <td class="day"><span class="day_wrap">19</span></td>
                        <td class="day"><span class="day_wrap">20</span></td>
                    </tr>
                    <tr>
                        <td class="today"><span class="day_wrap">21</span></td>
                        <td class="day"><span class="day_wrap">22</span></td>
                        <td class="day"><span class="day_wrap">23</span></td>
                        <td class="day"><span class="day_wrap">24</span></td>
                        <td class="day"><span class="day_wrap">25</span></td>
                        <td class="day"><span class="day_wrap">26</span></td>
                        <td class="day"><span class="day_wrap">27</span></td>
                    </tr>
                    <tr>
                        <td class="day"><span class="day_wrap">28</span></td>
                        <td class="day"><span class="day_wrap">29</span></td>
                        <td class="day"><span class="day_wrap">30</span></td>
                        <td class="pad" colspan="4"><span class="day_wrap">&nbsp;</span></td>
                    </tr>
                    </tbody>
                </table>
            </aside>
            <!-- /Calendar widget -->
            <!-- Recent posts widget -->
            <aside class="column-1_3 widget">
                <h5 class="widget_title">Recent Posts</h5>
                <article class="post_item first">
                    <div class="post_thumb">
                        <img alt="Medical Chemistry: The Molecular Basis" src="public/layout/images/masonry_01-75x75.jpg">
                    </div>
                    <div class="post_content">
                        <h6 class="post_title">
                            <a href="post-with-sidebar.html">Medical Chemistry: The Molecular Basis</a>
                        </h6>
                        <div class="post_info">
										<span class="post_info_item post_info_posted">
											<a href="#" class="post_info_date">January 14, 2015</a>
										</span>
                            <span class="post_info_item post_info_posted_by">by 
											<a href="#" class="post_info_author">John Doe</a>
										</span>
                            <span class="post_info_item post_info_counters">
											<a href="#" class="post_counters_item post_counters_views icon-eye"><span>157</span></a>
                                        </span>
                        </div>
                    </div>
                </article>
                <article class="post_item">
                    <div class="post_thumb">
                        <img alt="Introduction to Computer  Science" src="public/layout/images/masonry_02-75x75.jpg">
                    </div>
                    <div class="post_content">
                        <h6 class="post_title">
                            <a href="post-without-sidebar.html">Introduction to Computer  Science</a>
                        </h6>
                        <div class="post_info">
										<span class="post_info_item post_info_posted">
											<a href="#" class="post_info_date">January 14, 2015</a>
										</span>
                            <span class="post_info_item post_info_posted_by">by 
											<a href="#" class="post_info_author">John Doe</a>
										</span>
                            <span class="post_info_item post_info_counters">
											<a href="#" class="post_counters_item post_counters_views icon-eye"><span>103</span>
											</a>
                                        </span>
                        </div>
                    </div>
                </article>
                <article class="post_item ">
                    <div class="post_thumb">
                        <img alt="Introduction to Biomedical Imaging" src="public/layout/images/masonry_03-75x75.jpg">
                    </div>
                    <div class="post_content">
                        <h6 class="post_title">
                            <a href="post-without-sidebar.html">Introduction to Biomedical Imaging</a>
                        </h6>
                        <div class="post_info">
										<span class="post_info_item post_info_posted">
											<a href="#" class="post_info_date">January 13, 2015</a>
										</span>
                            <span class="post_info_item post_info_posted_by">by 
											<a href="#" class="post_info_author">John Doe</a>
										</span>
                            <span class="post_info_item post_info_counters">
											<a href="#" class="post_counters_item post_counters_views icon-eye"><span>80</span></a>
                                        </span>
                        </div>
                    </div>
                </article>
                <article class="post_item">
                    <div class="post_thumb">
                        <img alt="Evaluating Social Programs" src="public/layout/images/masonry_04-75x75.jpg"></div>
                    <div class="post_content">
                        <h6 class="post_title">
                            <a href="post-without-sidebar.html">Evaluating Social Programs</a>
                        </h6>
                        <div class="post_info">
										<span class="post_info_item post_info_posted">
											<a href="#" class="post_info_date">January 13, 2015</a>
										</span>
                            <span class="post_info_item post_info_posted_by">by 
											<a href="#" class="post_info_author">John Doe</a>
										</span>
                            <span class="post_info_item post_info_counters">
											<a href="#" class="post_counters_item post_counters_views icon-eye"><span>77</span></a>
                                        </span>
                        </div>
                    </div>
                </article>
            </aside>
            <!-- /Recent posts widget -->
            <!-- Recent comments widget -->
            <aside class="column-1_3 widget widget_recent_comments">
                <h5 class="widget_title">Latest comments</h5>
                <ul>
                    <li>
                        <span>TRX_admin</span> on
                        <a href="product-page.html">Star Print Backpack</a>
                    </li>
                    <li>
                        <span>TRX_admin</span> on
                        <a href="product-page.html">Yellow Backpack</a>
                    </li>
                    <li>
                        <span>Sebastian Jones</span> on
                        <a href="product-page.html">Principles of Written English, Part 2</a>
                    </li>
                    <li>
                        <span>TRX_admin</span> on
                        <a href="product-page.html">Principles of Written English, Part 2</a>
                    </li>
                    <li>
                        <span>TRX_admin</span> on
                        <a href="product-page.html">Video Training for Microsoft products and technologies</a>
                    </li>
                </ul>
            </aside>
            <!-- /Recent comments widget -->
        </div>
    </div>
</footer>
<!-- /Widgets Footer -->
<!-- Contacts Footer  -->
<footer class="contacts_wrap bg_tint_dark contacts_style_dark">
    <div class="content_wrap">
        <div class="logo">
            <a href="index-2.html">
                <img src="public/layout/images/logo_footer.png" alt="">
            </a>
        </div>
        <div class="contacts_address">
            <address class="address_right">
                Phone: 1.800.123.4567<br>
                Fax: 1.800.123.4566
            </address>
            <address class="address_left">
                San Francisco, CA 94102, US<br>
                1234 Some St
            </address>
        </div>
        <div class="sc_socials sc_socials_size_big">
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_facebook">
                    <span class="sc_socials_hover social_facebook"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_pinterest">
                    <span class="sc_socials_hover social_pinterest"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_twitter">
                    <span class="sc_socials_hover social_twitter"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_gplus">
                    <span class="sc_socials_hover social_gplus"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_rss">
                    <span class="sc_socials_hover social_rss"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_dribbble">
                    <span class="sc_socials_hover social_dribbble"></span>
                </a>
            </div>
        </div>
    </div>
</footer>