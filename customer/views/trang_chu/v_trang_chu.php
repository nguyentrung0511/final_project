<!-- Revolution slider -->
<section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_education_home_slider">
    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner disp_none height_630 max-height_630">
            <ul>
                <!-- Slide 1 -->
                <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                    <img src="public/layout/images/green.jpg" alt="green" data-bgposition="center top" data-bgfit="normal" data-bgrepeat="repeat">
                    <div class="tp-caption customin stl cust-z-index-5 rs-cust-style8" data-x="20" data-y="230" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:20;transformOrigin:50% 100%;" data-speed="1300" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8250" data-endspeed="300">
                        <img src="public/layout/images/13.jpg" alt="">
                    </div>
                    <div class="tp-caption title sfr stl tp-resizeme cust-z-index-6 rs-cust-style1" data-x="570" data-y="190" data-speed="500" data-start="1350" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8400" data-endspeed="300">Tham gia các khóa học tuyệt vời từ trung tâm tốt nhất Việt Nam
                    </div>
                </li>
                <!-- Slide 2 -->
                <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                    <img src="public/layout/images/blue.jpg" alt="blue" data-bgposition="center top" data-bgfit="normal" data-bgrepeat="repeat">
                    <div class="tp-caption customin stl cust-z-index-5 rs-cust-style8" data-x="40" data-y="200" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:360;scaleX:0.1;scaleY:0.1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" data-speed="1300" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8250" data-endspeed="300">
                        <img src="public/layout/images/slide-2-1.png" alt="">
                    </div>
                    <div class="tp-caption title sfb stb tp-resizeme cust-z-index-6 rs-cust-style1" data-x="570" data-y="200" data-speed="500" data-start="1350" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8400" data-endspeed="300">Các khóa học lập trình của chúng tôi được thiết kế theo yêu cầu của doanh nghiệp
                    </div>

                </li>
                <!-- Slide 3 -->
                <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                    <img src="public/layout/images/yellow.jpg" alt="yellow" data-bgposition="center top" data-bgfit="normal" data-bgrepeat="repeat">
                    <div class="tp-caption roundedimage sfl stl cust-z-index-5 rs-cust-style8" data-x="50" data-y="200" data-speed="1300" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8250" data-endspeed="300">
                        <img src="public/layout/images/slide-3-1.jpg" alt="">
                    </div>
                    <div class="tp-caption title customin stb tp-resizeme cust-z-index-6 rs-cust-style1" data-x="570" data-y="200" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:20;transformOrigin:50% 100%;" data-speed="500" data-start="1350" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8400" data-endspeed="300">Tự tin đi làm sau 4-6 tháng
                    </div>
                </li>
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>
<!-- Revolution slider -->
<!-- Content -->
<div class="page_content_wrap">
    <div class="content">
        <article class="post_item post_item_single page">
            <section class="post_content">
                <!-- Courses section -->
                <div class="sc_section accent_top bg_tint_light sc_bg1" data-animation="animated fadeInUp normal">
                    <div class="sc_section_overlay">
                        <div class="sc_section_content">
                            <div class="sc_content content_wrap margin_top_2_5em_imp margin_bottom_2_5em_imp">
                                <h2 class="sc_title sc_title_regular sc_align_center margin_top_0 margin_bottom_085em text_center">ĐÀO TẠO CHUYÊN SÂU</h2>
                                <div class="sc_blogger layout_courses_3 template_portfolio sc_blogger_horizontal no_description">
                                    <div class="isotope_wrap" data-columns="3">
                                        <!-- Courses item -->
                                        <?php
                                        foreach ( $chuyen_sau as $chuyen_sau){

                                        ?>
                                        <div class="isotope_item isotope_item_courses isotope_column_3">
                                            <div class="post_item post_item_courses odd">
                                                <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                                    <div class="post_featured img">
                                                        <a href="paid-course.html">
                                                            <img alt="Principles of Written English, Part 2" src="public/layout/images/masonry_15-400x400.jpg">
                                                        </a>
                                                        <h4 class="post_title">
                                                            <a href="#"><?php echo $chuyen_sau->ten_khoa_hoc ;?></a>
                                                        </h4>
                                                        <div class="post_descr">
                                                            <div class="post_price">
                                                                <span class="post_price_value"><?php echo $chuyen_sau->hoc_phi ;?>$</span>
                                                                <span class="post_price_period"><?php echo $chuyen_sau->thoi_gian ;?> tháng</span>
                                                            </div>
                                                            <div class="post_category">
                                                                <a href="product-category.html">Language</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="post_info_wrap info">
                                                        <div class="info-back">
                                                            <h4 class="post_title">
                                                                <a href="#"><?php echo $chuyen_sau->ten_khoa_hoc ;?></a>
                                                            </h4>
                                                            <div class="post_descr">
                                                                <div class="post_buttons">
                                                                    <div class="post_button">
                                                                        <a href="register.php" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">Đăng Ký</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <?php
                                        }
                                        ?>
                                        <!-- /Courses item -->
                                    </div>
                                </div>
                                <a href="courses-streampage.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_menu sc_button_size_small aligncenter sc_button_iconed icon-graduation margin_top_1em margin_bottom_4 widht_12em">VIEW ALL COURSES</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Courses section -->
                <!-- Courses section -->
                <div class="sc_section accent_top bg_tint_light sc_bg1" data-animation="animated fadeInUp normal">
                    <div class="sc_section_overlay">
                        <div class="sc_section_content">
                            <div class="sc_content content_wrap margin_top_2_5em_imp margin_bottom_2_5em_imp">
                                <h2 class="sc_title sc_title_regular sc_align_center margin_top_0 margin_bottom_085em text_center">ĐÀO TẠO CHUYÊN SÂU</h2>
                                <div class="sc_blogger layout_courses_3 template_portfolio sc_blogger_horizontal no_description">
                                    <div class="isotope_wrap" data-columns="3">
                                        <!-- Courses item -->
                                        <?php
                                        foreach ( $chuyen_de as $chuyen_de){

                                            ?>
                                            <div class="isotope_item isotope_item_courses isotope_column_3">
                                                <div class="post_item post_item_courses odd">
                                                    <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                                        <div class="post_featured img">
                                                            <a href="paid-course.html">
                                                                <img alt="Principles of Written English, Part 2" src="public/layout/images/masonry_15-400x400.jpg">
                                                            </a>
                                                            <h4 class="post_title">
                                                                <a href="#"><?php echo $chuyen_de->ten_khoa_hoc ;?></a>
                                                            </h4>
                                                            <div class="post_descr">
                                                                <div class="post_price">
                                                                    <span class="post_price_value"><?php echo $chuyen_de->hoc_phi ;?>$</span>
                                                                    <span class="post_price_period"><?php echo $chuyen_de->thoi_gian ;?> tháng</span>
                                                                </div>
                                                                <div class="post_category">
                                                                    <a href="product-category.html">Language</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="post_info_wrap info">
                                                            <div class="info-back">
                                                                <h4 class="post_title">
                                                                    <a href="#"><?php echo $chuyen_de->ten_khoa_hoc ;?></a>
                                                                </h4>
                                                                <div class="post_descr">
                                                                    <div class="post_buttons">
                                                                        <div class="post_button">
                                                                            <a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">Đăng Ký</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <!-- /Courses item -->
                                    </div>
                                </div>
                                <a href="courses-streampage.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_menu sc_button_size_small aligncenter sc_button_iconed icon-graduation margin_top_1em margin_bottom_4 widht_12em">VIEW ALL COURSES</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Courses section -->
                <!-- Pricing section -->
                <div class="sc_section accent_top bg_tint_light sc_bg1" data-animation="animated fadeInUp normal">
                    <div class="sc_section_overlay">
                        <div class="sc_section_content">
                            <div class="sc_content content_wrap margin_top_2_5em_imp margin_bottom_2_5em_imp">
                                <h2 class="sc_title sc_title_regular sc_align_center text_center margin_top_0 margin_bottom_085em">Plans &amp; Pricing</h2>
                                <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_3">
                                    <div class="column-1_3 sc_column_item sc_column_item_1 odd first text_center">
                                        <div class="sc_price_block sc_price_block_style_1 width_100per">
                                            <div class="sc_price_block_title">Trial</div>
                                            <div class="sc_price_block_money">
                                                <div class="sc_price_block_icon icon-clock-2"></div>
                                            </div>
                                            <div class="sc_price_block_description">
                                                <span class="sc_highlight font_2_57em lh_1em"><b>Free!</b> 30 Days</span>
                                            </div>
                                            <div class="sc_price_block_link">
                                                <a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">I WANT THIS PLAN</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-1_3 sc_column_item sc_column_item_2 even text_center">
                                        <div class="sc_price_block sc_price_block_style_2">
                                            <div class="sc_price_block_title">Monthly</div>
                                            <div class="sc_price_block_money">
                                                <div class="sc_price"><span class="sc_price_currency">$</span><span class="sc_price_money">89</span></div>
                                            </div>
                                            <div class="sc_price_block_description">
                                                <p><b>Save $98</b> every year compared to the monthly
                                                    <br /> plan by paying yearly.</p>
                                            </div>
                                            <div class="sc_price_block_link">
                                                <a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">I WANT THIS PLAN</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-1_3 sc_column_item sc_column_item_3 odd text_center">
                                        <div class="sc_price_block sc_price_block_style_3">
                                            <div class="sc_price_block_title">Yearly</div>
                                            <div class="sc_price_block_money">
                                                <div class="sc_price">
                                                    <span class="sc_price_currency">$</span>
                                                    <span class="sc_price_money">129</span>
                                                </div>
                                            </div>
                                            <div class="sc_price_block_description">
                                                <p><b>Save $120</b> every year compared to the monthly
                                                    <br /> plan by paying biannually.</p>
                                            </div>
                                            <div class="sc_price_block_link">
                                                <a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">I WANT THIS PLAN</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Pricing section -->
            </section>
        </article>
    </div>
</div>