<div class="page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1">
    <div class="content_wrap">
        <div class="breadcrumbs">
            <a class="breadcrumbs_item home" href="trang_chu.php">Home</a>
            <span class="breadcrumbs_delimiter"></span>
            <span class="breadcrumbs_item current">Đào tạo chuyên đề</span>
        </div>
        <h1 class="page_title">Đào tạo chuyên đề</h1>
    </div>
</div>
<!-- /Page title -->
<!-- Content with sidebar -->
<div class="page_content_wrap">
    <div class="content_wrap">
        <div class="content">
            <div class="isotope_wrap" data-columns="3">
                <!-- Courses item -->
                <?php
                foreach ( $chuyen_de1 as $chuyen_de2){

                    ?>
                    <div class="isotope_item isotope_item_courses isotope_column_3">
                        <div class="post_item post_item_courses odd">
                            <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                <div class="post_featured img">
                                    <a href="paid-course.html">
                                        <img alt="Principles of Written English, Part 2" src="public/layout/images/masonry_15-400x400.jpg">
                                    </a>
                                    <h4 class="post_title">
                                        <a href="#"><?php echo $chuyen_de2->ten_khoa_hoc ;?></a>
                                    </h4>
                                    <div class="post_descr">
                                        <div class="post_price">
                                            <span class="post_price_value"><?php echo $chuyen_de2->hoc_phi ;?>$</span>
                                            <span class="post_price_period"><?php echo $chuyen_de2->thoi_gian ;?> tháng</span>
                                        </div>
                                        <div class="post_category">
                                            <a href="product-category.html">Language</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="post_info_wrap info">
                                    <div class="info-back">
                                        <h4 class="post_title">
                                            <a href="#"><?php echo $chuyen_de2->ten_khoa_hoc ;?></a>
                                        </h4>
                                        <div class="post_descr">
                                            <div class="post_buttons">
                                                <div class="post_button">
                                                    <a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">Đăng Ký</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <!-- /Courses item -->
            </div>
            <div id="viewmore" class="pagination_wrap pagination_viewmore">
                <a href="#" id="viewmore_link" class="theme_button viewmore_button">
                    <span class="icon-spin3 animate-spin viewmore_loading"></span>
                    <span class="viewmore_text_1">LOAD MORE</span>
                    <span class="viewmore_text_2">Loading ...</span>
                </a>
                <span class="viewmore_loader"></span>
            </div>
        </div>
    </div>
</div>
<!-- /Content -->